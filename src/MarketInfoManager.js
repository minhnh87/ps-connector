import {transformMessage, makeRequest} from './utils';
import MessageTransformers from './transformers/MessageTransformers';
import decodeData from './MessageDecoder';
import {RoomNames} from './constants';
import { consume, stopConsume, onEvent } from './SocketManager';

const MarketInfoEndpoints = {
    marketInfoByFloorCodes: atob(
		'L21hcmtldC1pbmZvcy9zbmFwc2hvdD9mbG9vckNvZGU9'
	) /**    /market-infos/snapshot?floorCode=    **/,
	marketHistoryByFloorCodes: atob(
		'L21hcmtldC1pbmZvcy9oaXN0b3J5P2Zsb29yQ29kZT0='
	) /**    /market-infos/history?floorCode=    **/,
	ceilFloorCountByFloorCodes: atob(
		'L21hcmtldC1zdGF0cy9zbmFwc2hvdD9mbG9vcj0='
	) /**    /market-stats/snapshot?floor=    **/,
};

export function getMarketInfoByFloorCodes(floorCodes) {
    if (!floorCodes || floorCodes.length === 0) {
        throw new Error('Missing floorCodes');
    }

    return makeRequest({
        path: MarketInfoEndpoints.marketInfoByFloorCodes + floorCodes.join(',')
    }).then(response => {
        if (response.data) {
            return response.data.map(item => transformMessage(RoomNames.MARKET_INFO, item))
        } 
    }).catch(error => {
        return error;
    });
};


export function getCeilFloorCountByFloorCodes(floorCodes) {
    if (!floorCodes || floorCodes.length === 0) {
        throw new Error('Missing floorCodes');
    }

    return makeRequest({
        path: MarketInfoEndpoints.ceilFloorCountByFloorCodes + floorCodes.join(',')
    }).then(response => {
        if (response.data) {
            return response.data.map(item => MessageTransformers[RoomNames.MARKET_STAT].SNAPSHOT(decodeData(item)));
        } 
    }).catch(error => {
        return error;
    });
};

export function getMarketHistoryByFloorCodes(floorCodes) {
    if (!floorCodes || floorCodes.length === 0) {
        throw new Error('Missing floorCodes');
    }

    return makeRequest({
        path: MarketInfoEndpoints.marketHistoryByFloorCodes + floorCodes.join(',')
    }).then(response => {
        if (response.data) {
            return response.data.map(item => MessageTransformers[RoomNames.MARKET_INFO].MARKET_HISTORY(decodeData(item)));
        } 
    }).catch(error => {
        return error;
    });
};

export function consumeMarketInfos(floorCodes) {
    consume(RoomNames.MARKET_INFO, floorCodes);
    consume(RoomNames.MARKET_STAT, floorCodes);
}

export function onMarketInfoUpdate(updateHandler) {
    onEvent(RoomNames.MARKET_INFO, updateHandler);
}

export function onMarketStatUpdate(updateHandler) {
    onEvent(RoomNames.MARKET_STAT, updateHandler);
}

export function stopConsumeMarketInfos(floorCodes) {
    stopConsume(RoomNames.MARKET_INFO, floorCodes);
    stopConsume(RoomNames.MARKET_STAT, floorCodes);
}