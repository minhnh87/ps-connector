import {transformMessage, makeRequest} from './utils';
import {RoomNames} from './constants';
import { consume, stopConsume, onEvent, consumeQuery, stopConsumeQuery } from './SocketManager';

const StockEndpoints = {
    stockByCodes: atob(
		'L3N0b2Nrcy9zbmFwc2hvdD9jb2RlPQ=='
	) /**    /stocks/snapshot?code=    **/,
	stockByFloorCodes: atob(
		'L3N0b2Nrcy9zbmFwc2hvdD9mbG9vckNvZGU9'
	) /**    /stocks/snapshot?floorCode=    **/,
};


let stocks = {};
function setStockSnapshot(stock) {
    if (!stock || !stock.code) return;
    let symbol = stock.code;
    stocks[symbol] = stock;
}

function updateStock(stock) {
    if (!stock || !stock.code) return;
    let symbol = stock.code;
    if (stocks[symbol]) {
        stocks[symbol] = Object.assign({}, stocks[symbol], stock);
    } else {
        stocks[symbol] = stock;
    }
    return stocks[symbol];
}

export function getStocksBySymbols(symbols) {
    if (!symbols || symbols.length === 0) {
        throw new Error('Missing symbols');
    }

    return makeRequest({
        path: StockEndpoints.stockByCodes + symbols.join(','),
    }).then(response => {
        if (response.data) {
            let parsedData = response.data.map(stock => transformMessage(RoomNames.STOCK, stock));
            parsedData.forEach(item => {
                setStockSnapshot(item);
            });
            return parsedData;
        } 
    }).catch(error => {
        return error;
    });
};


export function getStocksByFloorCodes(floorCodes) {
    if (!floorCodes || floorCodes.length === 0) {
        throw new Error('Missing floorCodes');
    }

    return makeRequest({
        path: StockEndpoints.stockByFloorCodes + floorCodes.join(',')
    }).then(response => {
        if (response.data) {
            let parsedData = response.data.map(stock => transformMessage(RoomNames.STOCK, stock));
            parsedData.forEach(item => {
                setStockSnapshot(item);
            });
            return parsedData;
        } 
    }).catch(error => {
        return error;
    });
};

export function consumeStocksBySymbols(symbols) {
    consume(RoomNames.STOCK, symbols);
}

export function consumeStocks(query) {
    consumeQuery(RoomNames.STOCK, query);
}

export function onStockUpdate(updateHandler) {
    onEvent(RoomNames.STOCK, (stock) => {
        let newStock = updateStock(stock);
        updateHandler(newStock);
    });
}

export function stopConsumeStocksBySymbols(symbols) {
    stopConsume(RoomNames.STOCK, symbols);
}

export function stopConsumeStocks(query) {
    stopConsumeQuery(RoomNames.STOCK, query);
}