import {transformMessage, makeRequest} from './utils';
import MessageTransformers from './transformers/MessageTransformers';
import {RoomNames} from './constants';
import { consume, stopConsume, onEvent, consumeQuery, stopConsumeQuery } from './SocketManager';

const DerivativeEndpoints = {
    derivativeByCodes: atob(
		'L2Rlcml2YXRpdmVzL3NuYXBzaG90P2NvZGU9'
	) /**    /derivatives/snapshot?code=    **/,
	derivativeByFloorCodes: atob(
		'L2Rlcml2YXRpdmVzL3NuYXBzaG90P2Zsb29yQ29kZT0='
	) /**    /derivatives/snapshot?floorCode=    **/,
	derivativeByUnderlyings: atob(
		'L2Rlcml2YXRpdmVzL3NuYXBzaG90P3VuZGVybHlpbmc9'
	) /**    /derivatives/snapshot?underlying=    **/,
	derivativeTransactions: atob(
		'L2Rlcml2YXRpdmVzL3RyYW5zYWN0aW9ucz9zeW1ib2w9'
    ) /**    /derivatives/transactions?symbol=    **/,
    cumulativeVolumes:  atob(
        'L2Rlcml2YXRpdmVzL2N1bXVsYXRpdmUtdm9sdW1lcz9zeW1ib2w9'
    ) /**   /derivatives/cumulative-volumes?symbol= */
};

let derivatives = {};
function setDerivativeSnapshot(derivative) {
    if (!derivative || !derivative.code) return;
    let symbol = derivative.code;
    derivatives[symbol] = derivative;
}

function updateDerivative(derivative) {
    if (!derivative || !derivative.code) return;
    let symbol = derivative.code;
    if (derivatives[symbol]) {
        derivatives[symbol] = Object.assign({}, derivatives[symbol], derivative);
    } else {
        derivatives[symbol] = derivative;
    }
    return derivatives[symbol];
}

export function getDerivativesBySymbols(symbols) {
    if (!symbols || symbols.length === 0) {
        throw new Error('Missing symbols');
    }

    return makeRequest({
        path: DerivativeEndpoints.derivativeByCodes + symbols.join(',')
    }).then(response => {
        if (response.data) {
            let parsedData = response.data.map(derivative => transformMessage(RoomNames.DERIVATIVE, derivative));
            parsedData.forEach(item => {
                setDerivativeSnapshot(item);
            });
            return parsedData;
        } 
    }).catch(error => {
        return error;
    });
};

export function getDerivativesByFloorCodes(floorCodes) {
    if (!floorCodes || floorCodes.length === 0) {
        throw new Error('Missing floorCodes');
    }

    return makeRequest({
        path: DerivativeEndpoints.derivativeByFloorCodes + floorCodes.join(',')
    }).then(response => {
        if (response.data) {
            let parsedData = response.data.map(derivative => transformMessage(RoomNames.DERIVATIVE, derivative));
            parsedData.forEach(item => {
                setDerivativeSnapshot(item);
            });
            return parsedData;
        } 
    }).catch(error => {
        return error;
    });
};

export function getDerivativesByUnderlyings(underlyings) {
    if (!underlyings || underlyings.length === 0) {
        throw new Error('Missing underlyings');
    }

    return makeRequest({
        path: DerivativeEndpoints.derivativeByUnderlyings + underlyings.join(',')
    }).then(response => {
        if (response.data) {
            let parsedData = response.data.map(derivative => transformMessage(RoomNames.DERIVATIVE, derivative));
            parsedData.forEach(item => {
                setDerivativeSnapshot(item);
            });
            return parsedData;
        } 
    }).catch(error => {
        return error;
    });
};


export function getDerivativeTransactionsBySymbols(symbols) {
    if (!symbols || symbols.length === 0) {
        throw new Error('Missing symbols');
    }

    return makeRequest({
        path: DerivativeEndpoints.derivativeTransactions + symbols.join(',')
    }).then(response => {
        if (response.data) {
            let parsedData = symbols.reduce((accumulate, symbol) => {
                accumulate[symbol] = MessageTransformers[RoomNames.DERIVATIVE_TRANSACTION].TRANSACTION_HISTORY(response.data[symbol]);
                return accumulate;
            }, {});
            return parsedData;
        } 
    }).catch(error => {
        return error;
    });
};

export function getDerivativeCumulativeVolumesBySymbols(symbols) {
    if (!symbols || symbols.length === 0) {
        throw new Error('Missing symbols');
    }

    return makeRequest({
        path: DerivativeEndpoints.cumulativeVolumes + symbols.join(',')
    }).then(response => {
        if (response.data) {
            let parsedData = symbols.reduce((accumulate, symbol) => {
                accumulate[symbol] = response.data[symbol];
                return accumulate;
            }, {});
            return parsedData;
        } 
    }).catch(error => {
        return error;
    });
};

export function consumeDerivativesBySymbols(symbols) {
    consume(RoomNames.DERIVATIVE, symbols);
}

export function consumeDerivativeTransactionsBySymbols(symbols) {
    consume(RoomNames.DERIVATIVE_TRANSACTION, symbols);
}

export function consumeDerivativeCumulativeVolumes(symbols) {
    consume(RoomNames.DERIVATIVE_CUMULATIVE_VOLUME, symbols);
}

export function consumeDerivatives(query) {
    consumeQuery(RoomNames.DERIVATIVE, query);
}

export function onDerivativeUpdate(updateHandler) {
    onEvent(RoomNames.DERIVATIVE, (derivative) => {
        let newDerivative = updateDerivative(derivative);
        updateHandler(newDerivative);
    });
}

export function onDerivativeTransactionUpdate(updateHandler) {
    onEvent(RoomNames.DERIVATIVE_TRANSACTION, (derivativeTransaction) => {
        updateHandler(derivativeTransaction);
    });
}

export function onDerivativeCumulativeVolumeUpdate(updateHandler) {
    onEvent(RoomNames.DERIVATIVE_CUMULATIVE_VOLUME, (cumulativeVolume) => {
        updateHandler(cumulativeVolume);
    });
}

export function stopConsumeDerivativesBySymbols(symbols) {
    stopConsume(RoomNames.DERIVATIVE, symbols);
}

export function stopConsumeDerivativeTransactionsBySymbols(symbols) {
    stopConsume(RoomNames.DERIVATIVE_TRANSACTION, symbols);
}

export function stopConsumeDerivativeCumulativeVolumes(symbols) {
    stopConsume(RoomNames.DERIVATIVE_CUMULATIVE_VOLUME, symbols);
}

export function stopConsumeDerivatives(query) {
    stopConsumeQuery(RoomNames.DERIVATIVE, query);
}