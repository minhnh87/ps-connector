const MarketStatMessageTransformers = {
	MS: data => {
		return {
			floor: data[0],
			type: data[1],
			count: data[2],
			symbls: data[3],
			tradingDate: data[4]
		};
	},
	SNAPSHOT: data => {
		return {
			floor: data[0],
			floorCount: data[1],
			ceilingCount: data[2],
			tradingDate: data[3]
		};
	}
};

export default MarketStatMessageTransformers;
