import decodeData from '../MessageDecoder';
import { transformMessageArrayToObject } from '../utils';

const DerivativeTransactionTransformer = {
	TRANSACTION_HISTORY: dataString => {
		let parts = decodeData(dataString);
		let timeSeries = parts[1].split(',');
		let priceSeries = parts[2].split(',');
		let volSeries = parts[3].split(',');
		let accumulatedVolSeries = parts[4].split(',');
		let accumulatedValSeries = parts[5].split(',');
		let result = [];
		for (let i = 0; i < timeSeries.length; i++) {
			let transactionObj = {
				floorCode: parts[0],
				time: timeSeries[i],
				last: priceSeries[i],
				lastVol: volSeries[i],
				accumulatedVol: accumulatedVolSeries[i],
				accumulatedVal: accumulatedValSeries[i]
			};
			result.push(transactionObj);
		}
		return result;
	},
	DT: data => {
		return transformMessageArrayToObject(
			[
				'symbol',
				'time',
				'floorCode',
				'last',
				'lastVol',
				'accumulatedVol',
				'accumulatedVal',
			],
			data
			)
	}
};

export default DerivativeTransactionTransformer;
