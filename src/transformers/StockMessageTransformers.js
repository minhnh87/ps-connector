import { transformMessageArrayToObject } from '../utils';

const StockMessageTransformers = {
	SFU: data => {
		let stockType = data[1];
		if (stockType === 'ST') {
			// For Stocks from HNX/UPCOM, it can have 10 ticks
			return transformMessageArrayToObject(
				[
					'code',
					'stockType',
					// basic info
					'floorCode',
					'basicPrice',
					'floorPrice',
					'ceilingPrice',
					// bid-ask info
					'bidPrice01',
					'bidPrice02',
					'bidPrice03',
					'bidPrice04',
					'bidPrice05',
					'bidPrice06',
					'bidPrice07',
					'bidPrice08',
					'bidPrice09',
					'bidPrice10',
					'bidQtty01',
					'bidQtty02',
					'bidQtty03',
					'bidQtty04',
					'bidQtty05',
					'bidQtty06',
					'bidQtty07',
					'bidQtty08',
					'bidQtty09',
					'bidQtty10',
					'offerPrice01',
					'offerPrice02',
					'offerPrice03',
					'offerPrice04',
					'offerPrice05',
					'offerPrice06',
					'offerPrice07',
					'offerPrice08',
					'offerPrice09',
					'offerPrice10',
					'offerQtty01',
					'offerQtty02',
					'offerQtty03',
					'offerQtty04',
					'offerQtty05',
					'offerQtty06',
					'offerQtty07',
					'offerQtty08',
					'offerQtty09',
					'offerQtty10',
					'totalBidQtty',
					'totalOfferQtty',
					// match info
					'tradingSessionId',
					'buyForeignQtty',
					'sellForeignQtty',
					'highestPrice',
					'lowestPrice',
					'accumulatedVal',
					'accumulatedVol',
					'matchPrice',
					'matchQtty',
					'currentPrice',
					'currentQtty',
					'projectOpen',
					'totalRoom',
					'currentRoom'
				],
				data
			);
		} else if (stockType === 'W') {
			return transformMessageArrayToObject(
				[
					'code',
					'stockType',
					// basic info
					'floorCode',
					'basicPrice',
					'floorPrice',
					'ceilingPrice',
					'underlyingSymbol',
					'issuerName',
					'exercisePrice',
					'exerciseRatio',
					// bid-ask info
					'bidPrice01',
					'bidPrice02',
					'bidPrice03',
					'bidQtty01',
					'bidQtty02',
					'bidQtty03',
					'offerPrice01',
					'offerPrice02',
					'offerPrice03',
					'offerQtty01',
					'offerQtty02',
					'offerQtty03',
					'totalBidQtty',
					'totalOfferQtty',
					// match info
					'tradingSessionId',
					'buyForeignQtty',
					'sellForeignQtty',
					'highestPrice',
					'lowestPrice',
					'accumulatedVal',
					'accumulatedVol',
					'matchPrice',
					'matchQtty',
					'currentPrice',
					'currentQtty',
					'projectOpen',
					'totalRoom',
					'currentRoom'
				],
				data
			);
		} else {
			return transformMessageArrayToObject(
				[
					'code',
					'stockType',
					// basic info
					'floorCode',
					'basicPrice',
					'floorPrice',
					'ceilingPrice',
					// bid-ask info
					'bidPrice01',
					'bidPrice02',
					'bidPrice03',
					'bidQtty01',
					'bidQtty02',
					'bidQtty03',
					'offerPrice01',
					'offerPrice02',
					'offerPrice03',
					'offerQtty01',
					'offerQtty02',
					'offerQtty03',
					'totalBidQtty',
					'totalOfferQtty',
					// match info
					'tradingSessionId',
					'buyForeignQtty',
					'sellForeignQtty',
					'highestPrice',
					'lowestPrice',
					'accumulatedVal',
					'accumulatedVol',
					'matchPrice',
					'matchQtty',
					'currentPrice',
					'currentQtty',
					'projectOpen',
					'totalRoom',
					'currentRoom'
				],
				data
			);
		}
	},

	SBA: data => {
		let stockType = data[1];
		if (stockType === 'ST') {
			return transformMessageArrayToObject(
				[
					'code',
					'stockType',
					// bid-ask info
					'bidPrice01',
					'bidPrice02',
					'bidPrice03',
					'bidPrice04',
					'bidPrice05',
					'bidPrice06',
					'bidPrice07',
					'bidPrice08',
					'bidPrice09',
					'bidPrice10',
					'bidQtty01',
					'bidQtty02',
					'bidQtty03',
					'bidQtty04',
					'bidQtty05',
					'bidQtty06',
					'bidQtty07',
					'bidQtty08',
					'bidQtty09',
					'bidQtty10',
					'offerPrice01',
					'offerPrice02',
					'offerPrice03',
					'offerPrice04',
					'offerPrice05',
					'offerPrice06',
					'offerPrice07',
					'offerPrice08',
					'offerPrice09',
					'offerPrice10',
					'offerQtty01',
					'offerQtty02',
					'offerQtty03',
					'offerQtty04',
					'offerQtty05',
					'offerQtty06',
					'offerQtty07',
					'offerQtty08',
					'offerQtty09',
					'offerQtty10',
					'totalBidQtty',
					'totalOfferQtty'
				],
				data
			);
		}

		return transformMessageArrayToObject(
			[
				'code',
				'stockType',
				// bid-ask info
				'bidPrice01',
				'bidPrice02',
				'bidPrice03',
				'bidQtty01',
				'bidQtty02',
				'bidQtty03',
				'offerPrice01',
				'offerPrice02',
				'offerPrice03',
				'offerQtty01',
				'offerQtty02',
				'offerQtty03',
				'totalBidQtty',
				'totalOfferQtty'
			],
			data
		);
	},

	SMA: data => {
		return transformMessageArrayToObject(
			[
				'code',
				'stockType',
				// match info
				'tradingSessionId',
				'buyForeignQtty',
				'sellForeignQtty',
				'highestPrice',
				'lowestPrice',
				'accumulatedVal',
				'accumulatedVol',
				'matchPrice',
				'matchQtty',
				'currentPrice',
				'currentQtty',
				'projectOpen',
				'totalRoom',
				'currentRoom'
			],
			data
		);
	},

	SBS: data => {
		let stockType = data[1];
		if (stockType === 'W') {
			return transformMessageArrayToObject(
				[
					'code',
					'stockType',
					// match info
					'floorCode',
					'basicPrice',
					'floorPrice',
					'ceilingPrice',
					'underlyingSymbol',
					'issuerName',
					'exercisePrice',
					'exerciseRatio'
				],
				data
			);
		}
		return transformMessageArrayToObject(
			[
				'code',
				'stockType',
				// match info
				'floorCode',
				'basicPrice',
				'floorPrice',
				'ceilingPrice'
			],
			data
		);
	}
};

export default StockMessageTransformers;
