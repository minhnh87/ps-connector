import { transformMessageArrayToObject } from '../utils';

const CumulativeVolumeTransformer = {
	CV: data => {
		return transformMessageArrayToObject(
			[
				'symbol',
				'price',
				'volume',
			],
			data
		)
	}
};

export default CumulativeVolumeTransformer;
