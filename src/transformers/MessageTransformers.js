import { RoomNames } from '../constants';
import StockMessageTransformers from './StockMessageTransformers';
import MarketInfoMessageTransformers from './MarketInfoMessageTransformers';
import DerivativeMessageTransformers from './DerivativeMessageTransformers';
import DerivativeTransactionTransformers from './DerivativeTransactionTransformers';
import CumulativeVolumeTransformers from './CumulativeVolumeTransformers';
import MarketStatMessageTransformers from './MarketStatMessageTransformers';

const MessageTransformers = {
	[RoomNames.STOCK]: StockMessageTransformers,
	[RoomNames.DERIVATIVE]: DerivativeMessageTransformers,
	[RoomNames.MARKET_INFO]: MarketInfoMessageTransformers,
	[RoomNames.MARKET_STAT]: MarketStatMessageTransformers,
	[RoomNames.DERIVATIVE_TRANSACTION]: DerivativeTransactionTransformers,
	[RoomNames.DERIVATIVE_CUMULATIVE_VOLUME]: CumulativeVolumeTransformers
};

export default MessageTransformers;
