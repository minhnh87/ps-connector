import { transformMessageArrayToObject } from '../utils';

const MarketInfoMessageTransformers = {
	MI: data => {
		return transformMessageArrayToObject(
			[
				'floorCode',
				'tradingTime',
				'status',
				'advance',
				'noChange',
				'decline',
				'marketIndex',
				'priorMarketIndex',
				'highestIndex',
				'lowestIndex',
				'totalShareTraded',
				'totalValueTraded',
				'totalNormalTradedQttyRd',
				'totalNormalTradedValueRd',
				'predictionIndex'
			],
			data
		);
	},

	MARKET_HISTORY: data => {
		return {
			floorCode: data[0],
			tradingDate: data[1],
			priorMarketIndex: data[2],
			tradingTime: !data[3] ? [] : data[3].split(','),
			marketIndex: !data[4] ? [] : data[4].split(',').map(i => i * 1),
			totalShareTraded: !data[5] ? [] : data[5].split(','),
			status: !data[6] ? [] : data[6].split(',')
		};
	}
};

export default MarketInfoMessageTransformers;
