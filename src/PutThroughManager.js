import {makeRequest} from './utils';

const PutThroughEndpoints = {
    putThroughTransactions: atob(
		'L3B1dC10aHJvdWdoL3RyYW5zYWN0aW9ucz9mbG9vckNvZGU9'
	) /**    /put-through/transactions?floorCode=    **/,
	putThroughOrders: atob(
		'L3B1dC10aHJvdWdoL29yZGVycz9mbG9vckNvZGU9'
	) /**    /put-through/orders?floorCode=    **/
};

export function getPutThroughTransactions (floorCodes) {
    if (!floorCodes || floorCodes.length === 0) {
        throw new Error('Missing floorCodes');
    }

    return makeRequest({
        path: PutThroughEndpoints.putThroughTransactions + floorCodes.join(',')
    }).then(response => {
        if (response.data) {
            return response.data
        } 
    }).catch(error => {
        return error;
    });
};

export function getPutThroughOrders(floorCodes) {
    if (!floorCodes || floorCodes.length === 0) {
        throw new Error('Missing floorCodes');
    }

    return makeRequest({
        path: PutThroughEndpoints.putThroughOrders + floorCodes.join(',')
    }).then(response => {
        if (response.data) {
            return response.data;
        } 
    }).catch(error => {
        return error;
    });
};
