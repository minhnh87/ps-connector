import { makeMessage, transformMessage } from './utils';
import { Types } from './constants';
import { getPriceSocketUrl } from './ServerManager';
import { setKey } from './MessageDecoder';


let socketClient;
let pollInterval;
let eventHandlers = {};
let hasAliveConnection = false;
let connectedFirstTime = false;
let registeredCodes = {};
let registeredQueries = {};
let reconnectCount = 0;


function isConnected() {
    return socketClient && socketClient.readyState === 1;
}

function createConnect() {
    if (isConnected()) return;
    socketClient = new WebSocket(getPriceSocketUrl());
    
    listenSocketEvents();
}

function clearConnect() {
    onSocketClose();
}


function pollConnectHealth() {
    if (pollInterval) return;
    pollInterval = setInterval(() => {
        if (!window.navigator.onLine) {
            clearConnect();
        } else {
            console.log(hasAliveConnection);
            if (!hasAliveConnection) {
                if (reconnectCount < 30) {
                    clearConnect();
                    createConnect();
                } else {
                    clearInterval(pollInterval);
                }
            }
        }
    }, 30e3);
}

function stopPollConnectHealth() {
    if (pollInterval) {
        clearInterval(pollInterval);
    }
}

function listenWindowEvents() {
    window.onbeforeunload = () => {
        stopListenSocketEvents();
    };
    window.addEventListener('online',  () => {
        createConnect();
    });
    window.addEventListener('offline', () => {
        clearConnect();
    });
}

function onSocketOpen() {
    hasAliveConnection = true;

    socketClient.send(makeMessage(Types.ACKNOWLEDGE));

    // consume already registered codes, useful for Reconnect case
    consumeAfterConnect();
    if (eventHandlers.open) {
        eventHandlers.open();
    }
};

function onSocketMessage(message) {
    hasAliveConnection = true;
    if (typeof message.data === 'string') {
        let messageSeparatorIndex = message.data.indexOf('|');
        let msgType, msgPayload;

        if (messageSeparatorIndex === -1) {
            msgType = message.data;
        } else {
            (msgType = message.data.substring(0, messageSeparatorIndex)),
                (msgPayload = message.data.slice(
                    messageSeparatorIndex + 1
                ));
        }

        switch (msgType) {
            case Types.DATA: {
                let payloadSeparatorIndex = msgPayload.indexOf(':');
                let roomName = msgPayload.substring(0, payloadSeparatorIndex),
                    msgString = msgPayload.slice(payloadSeparatorIndex + 1);
                let msgObj = transformMessage(roomName, msgString);
                if (eventHandlers[roomName] && typeof eventHandlers[roomName] === 'function') {
                    eventHandlers[roomName](msgObj);
                }

                break;
            }

            case Types.VERSION: {
                let version = msgPayload;
                console.log(
                    `%cPrice Socket V2 Connected %s, version %s`,
                    'color: green; font-weight: bold; font-family: Arial',
                    getPriceSocketUrl(),
                    version
                );
                break;
            }

            case Types.ENCODE_KEY: {
                setKey(msgPayload);
                break;
            }

            case Types.ERROR: {
                stopPollConnectHealth();
                break;
            }

            case Types.RESET: {
                if (eventHandlers && typeof eventHandlers.reset === 'function') {
                    eventHandlers.reset();
                }
                break;
            }

            case Types.CONNECTION: {
                socketClient.connectionId = msgPayload;
                console.log('connectionId', socketClient.connectionId);
                if (!connectedFirstTime) {
                    connectedFirstTime = true;
                    reconnectCount = 0;
                    console.log('connect first time');
                } else {
                    reconnectCount++;
                    console.log('reconnect', reconnectCount);
                }

                break;
            }

            case Types.PING: {
                socketClient.send(makeMessage(Types.PONG));
                break;
            }

            default: {
                break;
            }

        }

    } 
};

function onSocketError() {
    clearConnect();
    if (eventHandlers.error) {
        eventHandlers.error();
    }
};

function onSocketClose() {
    hasAliveConnection = false;
    if (socketClient && socketClient.connectionId) {
        console.log('Socket close:', socketClient.connectionId);
        stopListenSocketEvents();
        if (eventHandlers.close) {
            eventHandlers.close();
        }
        socketClient = null;
    }
};

function listenSocketEvents() {
    if (!socketClient) return;
    socketClient.addEventListener('open', onSocketOpen);
    socketClient.addEventListener('message', onSocketMessage);
    socketClient.addEventListener('error', onSocketError);
    socketClient.addEventListener('close', onSocketClose);
}

function stopListenSocketEvents() {
    if (!socketClient) return;
    socketClient.removeEventListener('open', onSocketOpen);
    socketClient.removeEventListener('message', onSocketMessage);
    socketClient.removeEventListener('error', onSocketError);
    socketClient.removeEventListener('close', onSocketClose);
}

function consumeAfterConnect() {
    if (socketClient && socketClient.readyState === 1) {
        for (let type in registeredCodes) {
            if (
                !registeredCodes[type] ||
                registeredCodes[type].size === 0
            )
                continue;

            socketClient.send(
                makeMessage(Types.SUBSCRIBE, {
                    name: type,
                    codes: Array.from(registeredCodes[type])
                })
            );
        }

        for (let type in registeredQueries) {
            if (!registeredQueries[type] || registeredQueries[type].size == 0) continue;
            socketClient.send(makeMessage(Types.SUBSCRIBE_QUERY, {
                name: type,
                query: registeredQueries[type]
            }))
        }
    }
}


export function connect() {
    createConnect();
    listenWindowEvents();
    pollConnectHealth();
}

export function disconnect() {
    clearConnect();
    stopPollConnectHealth();
}

export function onEvent(event, handler) {
    eventHandlers[event] = handler;
}

export function offEvent(event) {
    if (eventHandlers[event]) {
        delete eventHandlers[event];
    }
}

export function consume(type, codes) {
    if (!registeredCodes[type]) {
        registeredCodes[type] = new Set();
    }
    let codesToSubscribe = [];
    codes.map(code => {
        if (!registeredCodes[type].has(code)) {
            registeredCodes[type].add(code);
            codesToSubscribe.push(code);
        }
    });

    if (
        codesToSubscribe.length > 0 &&
        isConnected()
    ) {
        socketClient.send(
            makeMessage(Types.SUBSCRIBE, {
                name: type,
                codes: codesToSubscribe
            })
        );
    }
}

export function stopConsume(type, codes) {
    if (!registeredCodes[type]) return;
    let codesToUnubscribe = [];
    
    codes.forEach(code => {
        if (
            registeredCodes[type] &&
            registeredCodes[type].has(code)
        ) {
            registeredCodes[type].delete(code);
            codesToUnubscribe.push(code);
        }
    });

    if (isConnected() && codesToUnubscribe.length > 0) {
        socketClient.send(
            makeMessage(Types.UNSUBSCRIBE, {
                name: type,
                codes: codesToUnubscribe
            })
        );
    }
}

function isValidQuery(query) {
    return /([\w-]+(=[\w-]*)?(&[\w-]+(=[\w-]*)?)*)?$/gi.test(query);
}

export function consumeQuery(type, query) {
    if (!query || !type) {
        throw new Error("Missing type or query");
    }
    if (!registeredQueries[type]) {
        registeredQueries[type] = query;
    }

    if (
        isValidQuery(query) &&
        isConnected()
    ) {
        socketClient.send(
            makeMessage(Types.SUBSCRIBE_QUERY, {
                name: type,
                query
            })
        );
    }
}


export function stopConsumeQuery(type, query) {
    if (!registeredQueries[type]) return;
    registeredQueries[type] = null;

    if (isConnected() && isValidQuery(query)) {
        socketClient.send(
            makeMessage(Types.UNSUBSCRIBE_QUERY, {
                name: type,
                query
            })
        );
    }
}

