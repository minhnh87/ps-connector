let PriceApiUrl = 'https://price-api.vndirect.com.vn';
let PriceSocketUrl = 'wss://price-api.vndirect.com.vn/realtime';

export const getPriceApiUrl = () => {
    return PriceApiUrl;
}

export const getPriceSocketUrl = () => {
    return PriceSocketUrl;
}

export const setPriceEnv = (env) => {
    switch (env) {
        case 'uat': {
            PriceApiUrl = 'https://priceservice-v2-suat.vndirect.com.vn';
            PriceSocketUrl = 'wss://priceservice-v2-suat.vndirect.com.vn/realtime';
            break;
        }
        case 'priv': {
            PriceApiUrl = 'https://price-api-priv.vndirect.com.vn';
            PriceSocketUrl = 'wss://price-api-priv.vndirect.com.vn/realtime';
            break;
        }
        case 'prod': {
            PriceApiUrl = 'https://price-api.vndirect.com.vn';
            PriceSocketUrl = 'wss://price-api.vndirect.com.vn/realtime';
            break;
        }
        default: {
            PriceApiUrl = 'https://price-api.vndirect.com.vn';
            PriceSocketUrl = 'wss://price-api.vndirect.com.vn/realtime';
            break;
        }
    }
}
