import {connect, disconnect, onEvent} from './SocketManager';
import * as StockManager from './StockManager';
import * as DerivativeManager from './DerivativeManager';
import * as MarketInfoManager from './MarketInfoManager';
import * as PutThroughManager from './PutThroughManager';
import {setPriceEnv} from './ServerManager';

const PriceService = {
    version: process.env.VERSION,
    connect, disconnect, onEvent,
    ...StockManager,
    ...DerivativeManager,
    ...MarketInfoManager,
    ...PutThroughManager,
    setPriceEnv
};

window.PriceService = PriceService;

export default PriceService;
