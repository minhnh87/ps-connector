import axios from 'axios';
import {Types} from './constants';
import decodeData, { setKey } from './MessageDecoder';
import MessageTransformers from './transformers/MessageTransformers';
import { getPriceApiUrl } from './ServerManager';


export const makeMessage = (type, payload) => {
	switch (type) {
		case Types.SUBSCRIBE:
		case Types.UNSUBSCRIBE: {
			return `${type}|${payload.name}:${payload.codes.join(',')}`;
		}

		case Types.SUBSCRIBE_QUERY:
		case Types.UNSUBSCRIBE_QUERY: {
			return `${type}|${payload.name}:${payload.query}`;
		}

		default: {
			if (!payload) {
				return type;
			}
			if (typeof payload === 'string' || typeof payload == 'number') {
				return `${type}|${payload}`;
			}
			return `${type}|${JSON.stringify(payload)}`;
		}
	}
}


export const transformMessage = (roomName, messageString) => {
	let dataArr = decodeData(messageString);
	let type = dataArr[0];
	let data = dataArr.slice(1);
	if (
		MessageTransformers[roomName] &&
		typeof MessageTransformers[roomName][type] === 'function'
	) {
		return MessageTransformers[roomName][type](data);
	}
	return { type, data };
};


export const transformMessageArrayToObject = (fields, data) => {
	if (!data) return;
	return fields.reduce((accumulate, field, index) => {
		accumulate[field] = data[index];
		return accumulate;
	}, {});
};

export function makeRequest(request) {
    return axios({
        url: getPriceApiUrl() + request.path,
       
    }).then(res => {
		if (res && res.headers['x-ps-key']) {
			setKey(res.headers['x-ps-key']);
		}
		return res;
	}).catch(error => {
        if (error && error.response) {
            throw error.response;
        }
    });
}