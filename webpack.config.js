/* global __dirname, require, module*/

const webpack = require('webpack');
const path = require('path');
const env = require('yargs').argv.env; // use --env with webpack 2
const pkg = require('./package.json');
const MinifyPlugin = require('uglifyjs-webpack-plugin');

let libraryName = 'PriceService';

let outputFile, mode;

if (env === 'build') {
  mode = 'production';
  if (process.env.TARGET === 'private') {
    outputFile = libraryName + '.min.js';
  } else {
    outputFile = libraryName + '.3rdparty.min.js';
  }
} else {
  mode = 'development';
  outputFile = libraryName + '.js';
}

function getClientEnvironmenVars() {
  return {
    'process.env': {
      'VERSION': JSON.stringify(pkg.version)
    }
  }
}

const config = {
  mode: mode,
  entry: __dirname + '/src/index.js',
  output: {
    path: __dirname + '/lib',
    filename: outputFile,
    library: libraryName,
    libraryTarget: 'umd',
    umdNamedDefine: true,
    globalObject: "typeof self !== 'undefined' ? self : this"
  },
  module: {
    rules: [
      {
        test: /(\.jsx|\.js)$/,
        use: ['babel-loader', 'webpack-conditional-loader'],
        exclude: /(node_modules|bower_components)/
      },
      {
        test: /(\.jsx|\.js)$/,
        loader: 'eslint-loader',
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    modules: [path.resolve('./node_modules'), path.resolve('./src')],
    extensions: ['.json', '.js']
  },
  optimization: {
    minimizer: [
      new MinifyPlugin({
        extractComments: false,
        uglifyOptions: {
          mangle: true,
          keep_classnames: false,
          keep_fnames: false,
          ie8: false
          
        }
      })
    ],
    minimize: true
  },
  plugins: [
    new webpack.DefinePlugin(getClientEnvironmenVars())
  ]
};

module.exports = config;
