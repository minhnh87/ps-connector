# Price Service Client Library - Thư viện lấy thông tin thị trường

## Hỗ trợ
Hiện tại thư viện chỉ hỗ trợ chạy trên browser

## Cài đặt
Do đây là thư viện nội bộ cho VNDS, nên sẽ được quản lí qua Git để thừa hưởng việc phân quyền repo của Git.

### Thêm `devDependencies` trong `package.json`

```
"devDependencies": {
    ...
    "install-private-git": "git+https://gitlab.vndirect.com.vn/anh.lamminh/install-private-git.git"
}
```

Thư viện **install-private-git** sẽ giúp cài đặt các repo git nội bộ

### Thêm `gitDomain` và `gitDependencies` trong `package.json`

```
"dependencies": {...}
"devDependencies": {...}
"gitDomain": "gitlab.vndirect.com.vn",
"gitDependencies": {
    "price-service": "online-platform/price-service-connector-js#{version}"
}
```

### Thêm script trong `package.json` để install các repo git nội bộ

```
"scripts": {
    ...
    "postinstall": "node node_modules/install-private-git"
}
```

### Chạy `yarn` hoặc `npm install` để tiến hành cài đặt

Sau khi cài đặt thành công, thư viện `price-service` sẽ được cài vào `node_modules`


## Kết nối đến socket server
```
import PriceService from 'price-service';
PriceService.connect();
```

## Dữ liệu chỉ số index
### getMarketInfoByFloorCodes(Array floorCodes)
Lấy thông tin snapshot chỉ số index theo floorCode

```
PriceService.getMarketInfoByFloorCodes(['11'])
    .then(marketInfos => {
        // do something with marketInfos
        [
            {
                "floorCode": "11",
                "tradingTime": "15:02:06",
                "status": "13",
                "advance": "9",
                "noChange": "2",
                "decline": "19",
                "marketIndex": "695.74",
                "priorMarketIndex": "697.85",
                "highestIndex": "701.77",
                "lowestIndex": "687.18",
                "totalShareTraded": "15145838.3",
                "totalValueTraded": "2317.746".
                "totalNormalTradedQttyRd": "250391",
                "totalNormalTradedValueRd": "125317.248"
            }
        ]
    });
```

Danh sách một số floorCodes của các chỉ số:
- **10**: VN-INDEX
- **11**: VN30-INDEX
- **02**: HNX-INDEX
- **12**: HNX30-INDEX
- **03**: UPCOM-INDEX
- **13**: VNXALLSHARE-INDEX
- ... tất cả các floorCode mà Kafka trả ra thì PriceService cũng sẽ có dữ liệu

### consumeMarketInfos(Array floorCodes)
Đăng ký lắng nghe realtime thông tin chỉ số index theo floorCode

### onMarketInfoUpdate(Function updateHandler)
Nhận thông tin realtime chỉ số index

### stopConsumeMarketInfos(Array floorCodes)
Dừng lắng nghe thông tin realtime chỉ số index

```
PriceService.consumeMarketInfos(['10', '11', '12']);
PriceService.onMarketInfoUpdate(marketInfo => {
    // Do something with the updated info
});
```


## Dữ liệu cổ phiếu
### getStocksBySymbols(Array symbols)
Lấy snapshot thông tin mã cổ phiếu (theo mã)

```
PriceService.getStocksBySymbols(['VND'])
    .then(stocks => {
        /* do something with stocks
        [
            {
                "code": "VND",
                "stockType": "S",
                "floorCode": "10",
                "basicPrice": "11.5",
                "floorPrice": "10.7",
                "ceilingPrice": "12.3",
                "bidPrice01": "11.5",
                "bidPrice02": "11.45",
                "bidPrice03": "11.4",
                "bidQtty01": "5518",
                "bidQtty02": "330",
                "bidQtty03": "2827",
                "offerPrice01": "11.55",
                "offerPrice02": "11.6",
                "offerPrice03": "11.65",
                "offerQtty01": "2071",
                "offerQtty02": "3646",
                "offerQtty03": "2745",
                "totalBidQtty": "",
                "totalOfferQtty": "",
                "tradingSessionId": "",
                "buyForeignQtty": "9088",
                "sellForeignQtty": "38000",
                "highestPrice": "11.65",
                "lowestPrice": "11.2",
                "accumulatedVal": "11.638",
                "accumulatedVol": "102060",
                "matchPrice": "11.5",
                "matchQtty": "7296",
                "currentPrice": "",
                "currentQtty": "",
                "projectOpen": "11.5",
                "totalRoom": "10801078.2",
                "currentRoom": "2322766.7"
            }
        ] */
    });
```

### getStocksByFloorCodes(Array floorCodes)
Lấy snapshot thông tin mã cổ phiếu (theo sàn)

### consumeStocks(Array symbols)
Đăng ký lắng nghe realtime thông tin cổ phiếu theo mã

### onStockUpdate(Function updateHandler)
Nhận thông tin realtime mã cổ phiếu

### stopConsumeStocks(Array symbols)
Dừng lắng nghe thông tin realtime mã cổ phiếu

```
PriceService.consumeStocks(['VND', 'SSI', 'VPB']);
PriceService.onStockUpdate(stock => {
    // Do something with the updated info
});
```


## Dữ liệu phái sinh

### getDerivativesBySymbols(Array symbols)
Lấy snapshot thông tin mã hợp đồng phái sinh (theo mã)

```
PriceService.getDerivativesBySymbols(['VN30F2004'])
    .then(derivatives => {
        /* do something with derivatives
        [
            {
                "code": "VN30F2004", 
                "time": "14:46:01",
                "bidPrice01": "0.6820",
                "bidPrice02": "0.6815",
                "bidPrice03": "0.6810",
                "bidPrice04": "0.6805",
                "bidPrice05": "0.6802",
                "bidPrice06": "0.6801",
                "bidPrice07": "0.6800",
                "bidPrice08": "0.6799",
                "bidPrice09": "0.6798",
                "bidPrice10": "0.6797",
                "bidQtty01": "3.7",
                "bidQtty02": "0.3",
                "bidQtty03": "1.6",
                "bidQtty04": "0.6",
                "bidQtty05": "0.2",
                "bidQtty06": "3.8",
                "bidQtty07": "3.7",
                "bidQtty08": "1.1",
                "bidQtty09": "0.1",
                "bidQtty10": "0.8",
                "offerPrice01": "0.6821",
                "offerPrice02": "0.6822",
                "offerPrice03": "0.6823",
                "offerPrice04": "0.6824",
                "offerPrice05": "0.6825",
                "offerPrice06": "0.6826",
                "offerPrice07": "0.6827",
                "offerPrice08": "0.6828",
                "offerPrice09": "0.6829",
                "offerPrice10": "0.6830",
                "offerQtty01": "0.3",
                "offerQtty02": "0.1",
                "offerQtty03": "0.3",
                "offerQtty04": "0.6",
                "offerQtty05": "0.8",
                "offerQtty06": "0.2",
                "offerQtty07": "0.2",
                "offerQtty08": "0.9",
                "offerQtty09": "0.3",
                "offerQtty10": "5.7",
                "totalBidQtty": "21409.4",
                "totalOfferQtty": "21361.9",
                "tradingSessionId": "CLOSED",
                "buyForeignQtty": "117.2",
                "sellForeignQtty": "118.9",
                "highestPrice": "0.686",
                "lowestPrice": "0.6721",
                "accumulatedVal": "14286721270000",
                "accumulatedVol": "21024.6",
                "matchPrice": "0.682",
                "currentPrice": "0.682",
                "matchQtty": "0.2",
                "currentQtty": "",
                "floorCode": "DER01",
                "stockType": "FU",
                "tradingDate": "1586476800000",
                "lastTradingDate": "20200416",
                "underlying": "VN30",
                "basicPrice": "0.6802",
                "floorPrice": "0.6326",
                "ceilingPrice": "0.7278",
                "openInterest": "2471.3",
                "openPrice": "0.6802"
            }
        ] */
    });
```


### getDerivativesByFloorCodes(Array floorCodes)
Lấy snapshot thông tin mã hợp đồng phái sinh (theo floorCode)

**floorCode** hiện tại chỉ có **DER01**

### getDerivativesByUnderlyings(Array underlyings)
Lấy snapshot thông tin mã hợp đồng phái sinh (theo mã tài sản cơ sở)

Danh sách mã tài sản cơ sở:
- **VN30**: mã hợp đồng tương lai chỉ số VN30
- **VGB5**: mã hợp đồng tương lai trái phiếu chính phủ

### getDerivativeTransactionsBySymbols(Array symbols)
Lấy dữ liệu lịch sử khớp lệnh của mã hợp đồng phái sinh

### consumeDerivatives(Array symbols)
Đăng ký lắng nghe realtime thông tin hợp đồng phái sinh

### onDerivativeUpdate(Function updateHandler)
Nhận thông tin realtime mã hợp đồng phái sinh

### stopConsumeDerivatives(Array symbols)
Dừng lắng nghe thông tin realtime mã hợp đồng phái sinh

```
PriceService.consumeDerivatives(['VN30F2004', 'VN30F2005']);
PriceService.onDerivativeUpdate(derivative => {
    // Do something with the updated info
});
```


